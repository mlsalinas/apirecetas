const express = require('express');
const mongoose = require('mongoose')
const userSchema = require('./models/user')
const recipeSchema = require('./models/recipe');
const error = require('mongoose/lib/error');
const app = express();

//Middleware
app.use(express.json())

// MongoDB connection
// mongoose.connect('mongodb://poli:password@localhost:27017/recipies?authSource=admin')
mongoose.connect('mongodb://poli:password@mongoRecetas2:27017/recipes?authSource=admin')
.then(() => console.log('conectado a Mongo'))
.catch((error) => console.error(error))

// Routes
app.get('/', function (req, res) {
    res.send('Hello World!');
  });
  
app.post('/new_user', (req,res) => {
    const user = userSchema(req.body)
    user.save()
    .then((data) => res.json(data))
    .catch((error) => res.send(error));
})

app.post('/new_recipe', (req,res) => {
    const recipe = recipeSchema(req.body)
    recipe.save()
    .then((data) => res.json(data))
    .catch((error) => res.send(error));
})

app.post('/rate', (req,res) => {
    const {recipeId,userId,rating}= req.body
    recipeSchema.updateOne(
        { _id: recipeId },
        [ 
            { $set: { ratings: {$concatArrays: [{$ifNull: ['$ratings', []]}, [{ userId: userId, rating: rating}]]}} },
            {$set: {avgRating: {$trunc: [{$avg:['$ratings.rating']},0]}}}
        ]
    )
    .then((data) => res.json(data))
    .catch((error) =>{
        console.log(error)
        res.send(error) 
    });
})

app.get('/recipes/userId', (req,res) => {
    res.send({ userId });
})

app.get('/recipes', (req,res) => {
   const{userId,recipeId}= req.body
   if(recipeId){
    recipeSchema
        .find( { _id:recipeId} )
        .then ((data) => res.json(data))
        .catch((error)=> res.json({message:error}))
   }else{
        recipeSchema
        .find( { userId:userId} )
        .then ((data) => res.json(data))
        .catch((error)=> res.json({message:error}))
   }
})
app.get('/recipesbyingredient', async (req, res) => {
    const list_ingredients = req.body.ingredients.map((ingredient) => ingredient.name.toLowerCase());

    recipeSchema.aggregate([
            { $set: { es_subset: { $setIsSubset: [ "$ingredients.name", list_ingredients ] }} },
            { $match: {es_subset: true}},
            { $project: {es_subset: 0}}
        ]).exec()
            .then(resultados => {
                res.json(resultados)
            })
            .catch(err => {
                console.error(err);
            })
})



app.listen(3000, () => console.log("Esperando en puerto 3000..."))

